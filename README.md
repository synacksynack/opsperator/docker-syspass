# k8s Syspass

Syspass image.

Diverts from https://gitlab.com/synacksynack/opsperator/docker-php

Build with:

```
$ make build
```

Test with:

```
$ make syndemo
```

Start Demo or Cluster in OpenShift:

```
$ make ocdemo
$ make ocprod
```

Cleanup OpenShift assets:

```
$ make ocpurge
```

Environment variables and volumes
----------------------------------

The image recognizes the following environment variables that you can set during
initialization by passing `-e VAR=VALUE` to the Docker `run` command.

|    Variable name            |    Description             | Default                                                     | Inherited From    |
| :-------------------------- | -------------------------- | ----------------------------------------------------------- | ----------------- |
|  `ADMIN_PASSWORD`           | SysPass Admin Password     | `secret42`                                                  |                   |
|  `ADMIN_USER`               | SysPass Admin Username     | `syspassadmin`                                              |                   |
|  `APACHE_DOMAIN`            | Fusion VirtualHost         | `syspass.${OPENLDAP_DOMAIN}`                                | opsperator/apache |
|  `APACHE_HTTP_PORT`         | Fusion Directory HTTP Port | `8080`                                                      | opsperator/apache |
|  `MASTER_PASSWORD`          | SysPass Master Password    | `secret42secret42`                                          |                   |
|  `MYSQL_DATABASE`           | SysPass MySQL Database     | `syspass`                                                   |                   |
|  `MYSQL_HOST`               | SysPass MySQL Host         | `127.0.0.1`                                                 |                   |
|  `MYSQL_PASS`               | SysPass MySQL Password     | `secret`                                                    |                   |
|  `MYSQL_PORT`               | SysPass MySQL Port         | `3306`                                                      |                   |
|  `MYSQL_USER`               | SysPass MySQL Username     | `syspass`                                                   |                   |
|  `ONLY_TRUST_KUBE_CA`       | Don't trust base image CAs | `false`, any other value disables ca-certificates CAs       | opsperator/apache |
|  `OPENLDAP_BASE`            | OpenLDAP Base              | seds `OPENLDAP_DOMAIN`, default produces `dc=demo,dc=local` | opsperator/apache |
|  `OPENLDAP_BIND_DN_RREFIX`  | OpenLDAP Bind DN Prefix    | `cn=syspass,ou=services`                                    | opsperator/apache |
|  `OPENLDAP_BIND_PW`         | OpenLDAP Bind Password     | `secret`                                                    | opsperator/apache |
|  `OPENLDAP_DEFAULT_GROUP`   | SysPass default Group      | `1` (Admins, see `config/init.sql`)                         |                   |
|  `OPENLDAP_DEFAULT_PROFILE` | SysPass default Profile    | `1` (Admin, see `config/init.sql`)                          |                   |
|  `OPENLDAP_DOMAIN`          | OpenLDAP Domain Name       | `demo.local`                                                | opsperator/apache |
|  `OPENLDAP_HOST`            | OpenLDAP Backend Address   | `127.0.0.1`                                                 | opsperator/apache |
|  `OPENLDAP_PORT`            | OpenLDAP Bind Port         | `389` or `636` depending on `OPENLDAP_PROTO`                | opsperator/apache |
|  `OPENLDAP_PROTO`           | OpenLDAP Proto             | `ldap`                                                      | opsperator/apache |
|  `PHP_ERRORS_LOG`           | PHP Errors Logs Output     | `/proc/self/fd/2`                                           | opsperator/php    |
|  `PHP_MAX_EXECUTION_TIME`   | PHP Max Execution Time     | `30` seconds                                                | opsperator/php    |
|  `PHP_MAX_FILE_UPLOADS`     | PHP Max File Uploads       | `20`                                                        | opsperator/php    |
|  `PHP_MAX_POST_SIZE`        | PHP Max Post Size          | `8M`                                                        | opsperator/php    |
|  `PHP_MAX_UPLOAD_FILESIZE`  | PHP Max Upload File Size   | `2M`                                                        | opsperator/php    |
|  `PHP_MEMORY_LIMIT`         | PHP Memory Limit           | `-1` (no limitation)                                        | opsperator/php    |
|  `PUBLIC_PROTO`             | Apache Public Proto        | `http`                                                      | opsperator/apache |
|  `PW_SALT`                  | SysPass Passwords Salt     | generated during initial startup                            |                   |
|  `SITE_LANG`                | SysPass Language           | `en_US`                                                     |                   |


You can also set the following mount points by passing the `-v /host:/container`
flag to Docker.

|  Volume mount point          | Description                     | Inherited From    |
| :--------------------------- | ------------------------------- | ----------------- |
|  `/certs`                    | Apache Certificate (optional)   | opsperator/apache |
|  `/var/www/html/app/backup`  | SysPass Backup                  |                   |
|  `/var/www/html/app/cache`   | SysPass Cache                   |                   |
|  `/var/www/html/app/config`  | SysPass Configuration           |                   |
|  `/var/www/html/app/temp`    | SysPass Temp Files              |                   |

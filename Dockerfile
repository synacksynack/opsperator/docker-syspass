FROM docker.io/composer:2.0 as bootstrap

ENV SYSPASS_REPO=https://github.com/nuxsmin/sysPass \
    SYSPASS_VERSION=3.2.2

RUN git clone --branch $SYSPASS_VERSION $SYSPASS_REPO \
    && composer install --ignore-platform-reqs --no-interaction --no-plugins \
	--no-scripts --prefer-dist --optimize-autoloader \
	--working-dir /app/sysPass \
    && rm -fr sysPass/.git* sysPass/.travis* sysPass/COPYING sysPass/INSTALL \
	sysPass/README.md

FROM opsperator/php

ARG DO_UPGRADE=
ENV LANG=en_US.UTF-8 \
    SYSPASS_VERSION=3.2.2

# Syspass image for OpenShift Origin

LABEL io.k8s.description="Syspass Passwords Manager" \
      io.k8s.display-name="Syspass $SYSPASS_VERSION" \
      io.openshift.expose-services="8080:http" \
      io.openshift.tags="syspass,passwords" \
      io.openshift.non-scalable="false" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-syspass" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      version="$SYSPASS_VERSION"

USER root
COPY config/* /
COPY --from=bootstrap /app/sysPass/ /var/www/html/
COPY --from=bootstrap /usr/bin/composer /usr/bin/

RUN set -ex \
    && rm -rf /var/lib/apt/lists/* \
    && apt-get -y update \
    && if test "$DO_UPGRADE"; then \
	echo "# Upgrade Base Image"; \
	apt-get -y upgrade; \
	apt-get -y dist-upgrade; \
    fi \
    && echo "# Install Syspass dependencies" \
    && apt-get -y --no-install-recommends install locales locales-all \
	mariadb-client ldap-utils openssl apache2-utils libpng16-16 \
	libjpeg62-turbo libwebp6 libgd3 libxml2 \
    && ( \
	echo es_ES.UTF-8 UTF-8 \
	echo en_US.UTF-8 UTF-8 \
	echo en_GB.UTF-8 UTF-8 \
	echo de_DE.UTF-8 UTF-8 \
	echo ca_ES.UTF-8 UTF-8 \
	echo fr_FR.UTF-8 UTF-8 \
	echo ru_RU.UTF-8 UTF-8 \
	echo pl_PL.UTF-8 UTF-8 \
	echo nl_NL.UTF-8 UTF-8 \
	echo pt_BR.UTF-8 UTF-8 \
	echo da.UTF-8 UTF-8 \
	echo it_IT.UTF-8 UTF-8 \
	echo fo.UTF-8 UTF-8 \
    ) >/etc/locale.gen \
    && echo LANG=en_US.UTF-8 >/etc/default/locale \
    && dpkg-reconfigure locales \
    && update-locale LANG=en_US.UTF-8 \
    && export LANG=en_US.UTF-8 \
    && sed -i "s|LANG=.*|LANG=en_US.UTF-8|g" /etc/apache2/envvars \
    && savedAptMark="$(apt-mark showmanual)" \
    && apt-get -y --no-install-recommends install libcurl4-openssl-dev \
	libfreetype6-dev libicu-dev libldap2-dev libmcrypt-dev libldb-dev \
	libmemcached-dev libpng-dev libpq-dev libxml2-dev libevent-dev \
	libzip-dev libwebp-dev libgmp-dev libmagickwand-dev libedit-dev \
	libreadline-dev libjpeg62-turbo-dev libonig-dev \
    && if test "$DO_DEBUG"; then \
	pecl install xdebug-2.9.6; \
    fi \
    && pecl install mcrypt-1.0.3 \
    && docker-php-ext-configure gd --with-freetype --with-jpeg --with-webp \
    && docker-php-ext-configure ldap --with-libdir="lib/$debMultiarch" \
    && docker-php-ext-install mbstring gd intl ldap opcache pcntl pdo_mysql \
	gettext readline xml \
    && docker-php-ext-enable gettext ldap mcrypt intl pdo_mysql \
    && if test "$DO_DEBUG"; then \
	docker-php-ext-enable xdebug; \
    fi \
    && echo "# Enabling Syspass Modules" \
    && a2enmod ldap authnz_ldap proxy_fcgi setenvif ssl rewrite \
    && if test "$DO_DEBUG"; then \
	mv /xdebug.ini /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini; \
    fi \
    && sed -i 's|!Checks::checkIsWindows()|0 == 1|' \
	./lib/SP/Config/ConfigUtil.php \
    && echo "# Fixing permissions" \
    && for dir in /var/www/html/app/config /var/www/html/app/backup \
	    /var/www/html/app/resources /var/www/html/app/temp \
	    /var/www/html/app/cache /usr/local/etc/php/conf.d; \
	do \
	    mkdir -p "$dir" 2>/dev/null \
	    && chown -R 1001:root "$dir" \
	    && chmod -R g=u "$dir"; \
	done \
    && for f in /var/www/html/app/modules/*/plugins /var/www/html/composer.json \
	    /var/www/html/composer.lock /var/www/html/vendor; \
	do \
	    chown -R 1001:root $f \
	    && chmod -R g=u $f; \
	done \
    && echo "# Cleaning up" \
    && apt-mark auto '.*' >/dev/null \
    && apt-mark manual $savedAptMark \
    && apt-get purge -y --auto-remove -o \
	APT::AutoRemove::RecommendsImportant=false \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /usr/share/doc /usr/share/man \
	/usr/src/php.tar.xz /etc/apache2/sites-enabled/* /etc/ldap/ldap.conf \
    && unset HTTP_PROXY HTTPS_PROXY NO_PROXY DO_UPGRADE http_proxy https_proxy

CMD "/usr/sbin/apache2ctl" "-D" "FOREGROUND"
ENTRYPOINT ["dumb-init","--","/run-syspass.sh"]
USER 1001

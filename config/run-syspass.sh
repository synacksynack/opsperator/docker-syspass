#!/bin/sh

if test "$DEBUG"; then
    set -x
fi
. /usr/local/bin/nsswrapper.sh

ADMIN_PASSWORD="${ADMIN_PASSWORD:-secret42}"
ADMIN_USER=${ADMIN_USER:-syspassadmin}
APACHE_HTTP_PORT=${APACHE_HTTP_PORT:-8080}
DO_HTTPS=0
DO_LDAP=0
DO_PROXY=1
DO_TLS_LDAP=0
HTTP_PROXY_HOST=${HTTP_PROXY_HOST:-}
HTTP_PROXY_PORT=${HTTP_PROXY_PORT:-3128}
MASTER_PASSWORD="${MASTER_PASSWORD:-secret42secret42}"
MYSQL_DATABASE=${MYSQL_DATABASE:-syspass}
MYSQL_HOST=${MYSQL_HOST:-127.0.0.1}
MYSQL_PASS=${MYSQL_PASS:-secret}
MYSQL_PORT=${MYSQL_PORT:-3306}
MYSQL_USER=${MYSQL_USER:-mysql}
OPENLDAP_BIND_DN_PREFIX="${OPENLDAP_BIND_DN_PREFIX:-cn=syspass,ou=services}"
OPENLDAP_BIND_PW="${OPENLDAP_BIND_PW:-}"
OPENLDAP_DEFAULT_GROUP=${OPENLDAP_DEFAULT_GROUP:-1}
OPENLDAP_DEFAULT_PROFILE=${OPENLDAP_DEFAULT_PROFILE:-1}
OPENLDAP_DOMAIN=${OPENLDAP_DOMAIN:-demo.local}
OPENLDAP_HOST=${OPENLDAP_HOST:-127.0.0.1}
OPENLDAP_PROTO=${OPENLDAP_PROTO:-ldap}
PUBLIC_PROTO=${PUBLIC_PROTO:-http}
SITE_LANG=${SITE_LANG:-en_US}
if test -z "$OPENLDAP_BASE"; then
    OPENLDAP_BASE=`echo "dc=$OPENLDAP_DOMAIN" | sed 's|\.|,dc=|g'`
fi
if test -z "$OPENLDAP_PORT" -a "$OPENLDAP_PROTO" = ldaps; then
    OPENLDAP_PORT=636
elif test -z "$OPENLDAP_PORT"; then
    OPENLDAP_PORT=389
fi
if test -z "$APACHE_DOMAIN"; then
    APACHE_DOMAIN=syspass.$OPENLDAP_DOMAIN
fi
if test "$PUBLIC_PROTO" = https; then
    DO_HTTPS=1
fi
if test "$OPENLDAP_BIND_PW"; then
    DO_LDAP=1
    if test "$OPENLDAP_PROTO" = ldaps; then
	DO_TLS_LDAP=1
    fi
fi
if test -z "$HTTP_PROXY_HOST"; then
    if test "$HTTP_PROXY"; then
	HTTP_PROXY_HOST=`echo "$HTTP_PROXY" | sed 's|http[s]*://||' | cut -d: -f1`
	if echo "$HTTP_PROXY" | grep -E ':[0-9]*$' >/dev/null; then
	    HTTP_PROXY_PORT=`echo "$HTTP_PROXY" | sed "s|.*$HTTP_PROXY_HOST:||"`
	elif echo "$HTTP_PROXY" | grep ^https:// >/dev/null; then
	    HTTP_PROXY_PORT=443
	else
	    HTTP_PROXY_PORT=80
	fi
	DO_PROXY=1
    elif test "$http_proxy"; then
	HTTP_PROXY_HOST=`echo "$http_proxy" | sed 's|http[s]*://||' | cut -d: -f1`
	if echo "$HTTP_PROXY" | grep -E ':[0-9]*$' >/dev/null; then
	    HTTP_PROXY_PORT=`echo "$http_proxy" | sed "s|.*$HTTP_PROXY_HOST:||"`
	elif echo "$HTTP_PROXY" | grep ^https:// >/dev/null; then
	    HTTP_PROXY_PORT=443
	else
	    HTTP_PROXY_PORT=80
	fi
	DO_PROXY=1
    fi
else
    DO_PROXY=1
fi
export APACHE_DOMAIN
export APACHE_HTTP_PORT
export APACHE_IGNORE_OPENLDAP=yay
#export OPENLDAP_BASE
#export OPENLDAP_BIND_DN_PREFIX
#export OPENLDAP_DOMAIN
#export OPENLDAP_HOST
export PUBLIC_PROTO
SSL_INCLUDE=no-ssl
. /usr/local/bin/reset-tls.sh

cpt=0
echo Waiting for MySQL backend ...
while true
do
    if echo SHOW TABLES | mysql -u "$MYSQL_USER" \
	    --password="$MYSQL_PASS" -h "$MYSQL_HOST" \
	    -P "$MYSQL_PORT" "$MYSQL_DATABASE" >/dev/null 2>&1; then
	echo " MySQL is alive!"
	break
    elif test "$cpt" -gt 25; then
	echo "Could not reach MySQL" >&2
	exit 1
    fi
    sleep 5
    echo MySQL is ... KO
    cpt=`expr $cpt + 1`
done
if test "$OPENLDAP_BIND_PW"; then
    echo Waiting for LDAP backend ...
    cpt=0
    while true
    do
	if LDAPTLS_REQCERT=never \
		ldapsearch -H $OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT \
		-D "$OPENLDAP_BIND_DN_PREFIX,$OPENLDAP_BASE" \
		-b "ou=users,$OPENLDAP_BASE" \
		-w "$OPENLDAP_BIND_PW" \
		uid >/dev/null 2>&1; then
	    echo " LDAP is alive!"
	    break
	elif test "$cpt" -gt 25; then
	    echo Could not reach OpenLDAP >&2
	    exit 1
	fi
	sleep 5
	echo LDAP is ... KO
	cpt=`expr $cpt + 1`
    done
fi

SVERS=`echo $SYSPASS_VERSION | sed 's|\([0-9]*\)\.\([0-9]*\)\.\([0-9]*\)\.\(.*\)|\1\2\3.\4|'`

if test "$XDEBUG_REMOTE" -a "$XDEBUG_KEY" \
	-a -s /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini; then
    sed -e "s|__XDEBUG_REMOTE_HOST__|$XDEBUG_REMOTE|" \
	-e "s|__XDEBUG_IDE_KEY__|$XDEBUG_KEY|" \
	/usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
elif test -s /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini; then
    rm /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
fi

if ! test -s /var/www/html/app/config/config.xml; then
    ADMIN_PWHASH=`htpasswd -bnBC 10 "" "$ADMIN_PASSWORD" | tr -d ':\n'`
    MASTER_PWHASH=`htpasswd -bnBC 10 "" "$MASTER_PASSWORD" | tr -d ':\n'`
    if test -z "$PW_SALT"; then
	PW_SALT=`cat /dev/urandom | tr -dc 'a-f0-9' | fold -w 60 | head -n 1`
    fi
    eval `php /hash-masterpw.php "$ADMIN_USER" "$ADMIN_PASSWORD" "$MASTER_PASSWORD" "$PW_SALT"`
    if test -z "$ADMIN_MPASS" -o -z "$ADMIN_MKEY"; then
	echo CRITICAL: Failed generating hashes out of initial data
    elif ! sed -e "s|ADMIN_USER|$ADMIN_USER|" \
	    -e "s|ADMIN_PWHASH|$ADMIN_PWHASH|" \
	    -e "s|ADMIN_MPASS|$ADMIN_MPASS|" \
	    -e "s|ADMIN_MKEY|$ADMIN_MKEY|" \
	    -e "s|MASTER_PWHASH|$MASTER_PWHASH|" \
	    -e "s|SVERS|$SVERS|" \
	    /init.sql | mysql -u "$MYSQL_USER" \
	    --password="$MYSQL_PASS" -h "$MYSQL_HOST" \
	    -P "$MYSQL_PORT" "$MYSQL_DATABASE" >/dev/null 2>&1; then
	echo "CRITICAL: failed initializing database"
    else
	echo INFO: Done initializing database
	if ! sed -e "s|APP_URL|$PUBLIC_PROTO://$APACHE_DOMAIN|" \
		-e "s|DBHOST|$MYSQL_HOST|" \
		-e "s|DBNAME|$MYSQL_DATABASE|" \
		-e "s|DBPASS|$MYSQL_PASS|" \
		-e "s|DBPORT|$MYSQL_PORT|" \
		-e "s|DBUSER|$MYSQL_USER|" \
		-e "s|DO_HTTPS|$DO_HTTPS|" \
		-e "s|DO_LDAP|$DO_LDAP|" \
		-e "s|DO_PROXY|$DO_PROXY|" \
		-e "s|DO_TLS_LDAP|$DO_TLS_LDAP|" \
		-e "s|HTTP_PROXY_HOST|$HTTP_PROXY_HOST|" \
		-e "s|HTTP_PROXY_PORT|$HTTP_PROXY_PORT|" \
		-e "s|LDAP_BASE|$OPENLDAP_BASE|" \
		-e "s|LDAP_BIND_DN|$OPENLDAP_BIND_DN_PREFIX,$OPENLDAP_BASE|" \
		-e "s|LDAP_BIND_PW|$OPENLDAP_BIND_PW|" \
		-e "s|LDAP_DEFAULT_GROUP|$OPENLDAP_DEFAULT_GROUP|" \
		-e "s|LDAP_DEFAULT_PROFILE|$OPENLDAP_DEFAULT_PROFILE|" \
		-e "s|LDAP_HOST|$OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT|" \
		-e "s|PW_SALT|$PW_SALT|" \
		-e "s|SITE_LANG|$SITE_LANG|" \
		-e "s|SVERS|$SVERS|" \
		/config.xml >/var/www/html/app/config/config.xml; then
	    cat <<EOF
CRITICAL: failed writing configuration, please fix!
   Database was initialized with:
APP_URL=$PUBLIC_PROTO://$APACHE_DOMAIN
DB_HOST=$MYSQL_HOST
DB_NAME=$MYSQL_DATABASE
DB_PASS=$MYSQL_PASS
DB_PORT=$MYSQL_PORT
DB_USER=$MYSQL_USER
DO_HTTPS=$DO_HTTPS
DO_LDAP=$DO_LDAP
DO_TLS_LDAP=$DO_TLS_LDAP
DO_PROXY=$DO_PROXY
HTTP_PROXY_HOST=$HTTP_PROXY_HOST
HTTP_PROXY_PORT=$HTTP_PROXY_PORT
LDAP_BASE=$OPENLDAP_BASE
LDAP_BIND_DN=$OPENLDAP_BIND_DN_PREFIX,$OPENLDAP_BASE
LDAP_BIND_PW=$OPENLDAP_BIND_PW
LDAP_HOST=$OPENLDAP_HOST:$OPENLDAP_PORT
PW_SALT=$PW_SALT
SITE_LANG=$SITE_LANG
SVERS=$SVERS
EOF
	else
	    echo INFO: Done initializing SysPass
	fi
    fi
elif ! grep $SVERS /var/www/html/app/config/config.xml >/dev/null; then
    echo WARNING: configuration version mismatches image
    echo might have an update pending ... to automate
fi
if ! test -s /var/www/html/app/config/pubkey.pem; then
    echo NOTICE: inits PKI
    php /generate-keys.php
fi
if ! test -s /var/www/html/app/cache/colors.cache; then
    #WTF?!
    touch /var/www/html/app/cache/colors.cache
fi

echo Generates SysPass VirtualHost Configuration
sed -e "s|SYSPASS_HOSTNAME|$APACHE_DOMAIN|g" \
    -e "s|HTTP_PORT|$APACHE_HTTP_PORT|g" \
    -e "s|SSL_TOGGLE_INCLUDE|$SSL_INCLUDE.conf|g" \
    /vhost.conf >/etc/apache2/sites-enabled/003-vhosts.conf

export RESET_TLS=false
unset ADMIN_PASSWORD ADMIN_USER MASTER_PASSWORD ADMIN_PWHASH MASTER_PWHASH \
    PW_SALT ADMIN_MPASS ADMIN_MKEY XDEBUG_REMOTE XDEBUG_KEY DO_HTTPS DO_LDAP \
    OPENLDAP_DEFAULT_GROUP OPENLDAP_DEFAULT_PROFILE DO_TLS_LDAP DO_PROXY \
    MYSQL_USER MYSQL_HOST MYSQL_DATABASE MYSQL_PORT MYSQL_PASS SITE_LANG \
    DO_PROXY SVERS SYSPASS_VERSION

. /run-apache.sh

<?php
require_once('/var/www/html/vendor/phpseclib/phpseclib/phpseclib/Math/BigInteger.php');
require_once('/var/www/html/vendor/phpseclib/phpseclib/phpseclib/Crypt/Hash.php');
require_once('/var/www/html/vendor/phpseclib/phpseclib/phpseclib/Crypt/RSA.php');
use phpseclib\Crypt\RSA;

$rsa = new RSA();
$keys = $rsa->createKey(1024);
$fppriv = fopen('/var/www/html/app/config/key.pem', 'w');
$len = fwrite($fppriv, $keys['privatekey']);
fclose($fppriv);
chmod('/var/www/html/app/config/key.pem', 0600);
print "INFO: wrote private key ($len bytes)\n";

$fppub = fopen('/var/www/html/app/config/pubkey.pem', 'w');
$len = fwrite($fppub, $keys['publickey']);
fclose($fppub);
print "INFO: wrote public key ($len bytes)\n";
?>

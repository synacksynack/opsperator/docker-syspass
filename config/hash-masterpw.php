<?php
require_once('/var/www/html/vendor/defuse/php-encryption/src/Core.php');
require_once('/var/www/html/vendor/defuse/php-encryption/src/Crypto.php');
require_once('/var/www/html/vendor/defuse/php-encryption/src/DerivedKeys.php');
require_once('/var/www/html/vendor/defuse/php-encryption/src/Encoding.php');
require_once('/var/www/html/vendor/defuse/php-encryption/src/Exception/CryptoException.php');
require_once('/var/www/html/vendor/defuse/php-encryption/src/Exception/WrongKeyOrModifiedCiphertextException.php');
require_once('/var/www/html/vendor/defuse/php-encryption/src/Key.php');
require_once('/var/www/html/vendor/defuse/php-encryption/src/KeyOrPassword.php');
require_once('/var/www/html/vendor/defuse/php-encryption/src/KeyProtectedByPassword.php');
require_once('/var/www/html/vendor/defuse/php-encryption/src/RuntimeTests.php');
use Defuse\Crypto\KeyProtectedByPassword;
use Defuse\Crypto\Crypto;

$userLogin  = isset($argv[1]) ? $argv[1] : "admin";
$userPass   = isset($argv[2]) ? $argv[2] : "secretsecretsecretsecretsecretsecretsecret";
$masterPass = isset($argv[3]) ? $argv[3] : "s3crets3crets3crets3crets3crets3crets3cret";
$salt       = isset($argv[4]) ? $argv[4] : "be5570f6809a460bcbc5625fd3274ee0184c4164fe8707a8147dfcc84ac5";

$keypw = $userPass.$userLogin.$salt;
$mkey  = KeyProtectedByPassword::createRandomPasswordProtectedKey($keypw)->saveToAsciiSafeString();
$k     = KeyProtectedByPassword::loadFromAsciiSafeString($mkey)->unlockkey($keypw);
$mpass = Crypto::encrypt((string)$masterPass, $k);

print "ADMIN_MKEY=$mkey\n";
print "ADMIN_MPASS=$mpass\n";
?>
